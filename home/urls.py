from django.urls import path
from home import views
urlpatterns = [
    path('', views.HomeTemplateView.as_view(), name='home'),
    path('counter/', views.counter, name='visit'),
    path('products/<int:pk>/', views.get_products_per_brand, name="products"),
    path('send_informations_report_by_mail/', views.send_informations_report_by_mail, name="send_informations_report_by_mail")
]