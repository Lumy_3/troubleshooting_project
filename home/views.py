from io import BytesIO
from xhtml2pdf import pisa
from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.template.loader import get_template
from django.views.generic import TemplateView

from information.models import Information
from products.models import Products


class HomeTemplateView(TemplateView):
    template_name = 'home/template_name.html'


def counter(request):
    visit_count = request.session.get('counter', 0)
    request.session['counter'] = visit_count + 1
    context = {
        'count': visit_count
    }
    return render(request, 'home/counter.html', context)


def get_products_per_brand(request, pk):
    all_products_per_brand = Products.objects.filter(brand_id=pk)
    context = {
        'all_products_per_brand': all_products_per_brand
    }
    return render(request, 'home/all_products_per_brand.html', context)


def send_informations_report_by_mail(request):
    subject, from_email, to = 'Activity Report', 'testdjango124@gmail.com', 'lumi_085@yahoo.com'
    text_content = 'hello'
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.send()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
