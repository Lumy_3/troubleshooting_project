from django.urls import path

from brand import views

urlpatterns = [
    path('manufacturer/', views.BrandCreateView.as_view(), name='new_brand'),
    path('list_of_manufacturer/', views.BrandListView.as_view(), name='list_brand'),
    path('update_brand/ <int:pk>', views.BrandUpdateView.as_view(), name='update'),
    path('delete_brand/<int:pk>', views.BrandDeleteView.as_view(), name='delete'),
    path('detail_brand/<int:pk>', views.BrandDetailView.as_view(), name='detail'),
]
