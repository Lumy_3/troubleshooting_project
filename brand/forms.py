from django import forms
from django.forms import TextInput
from brand.models import Brand


class BrandForm(forms.ModelForm):
    class Meta:
        model = Brand
        fields = '__all__'
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please insert a new manufacturer', 'class': 'form-control'}),
            'category': TextInput(
                attrs={'placeholder': 'Please insert a new category', 'class': 'form-control'}),
            'troubleshooting': TextInput(
                attrs={'placeholder': 'Please insert a troubleshooting details', 'class': 'form-control'}),
            'contacts_manufacturer': TextInput(
                attrs={'placeholder': 'Please insert manufacturer contacts'
                    , 'class': 'form-control'}),
            'email_manufacturer': TextInput(
                attrs={'placeholder': 'Please insert the manufacturer email'
                    , 'class': 'form-control'})
        }

    def __init__(self, *args, **kwargs):
        super(BrandForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        name = cleaned_data.get('name')
        all_products = Brand.objects.all()
        for product in all_products:
            if product.name == name:
                msg = 'This category is already saved'
                self._errors['name'] = self.error_class([msg])
