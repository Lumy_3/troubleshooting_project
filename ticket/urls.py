from django.urls import path
from ticket import views
urlpatterns = [
    path('ticket/', views.TicketCreateView.as_view(), name='create_ticket'),
    path('list_ticket/', views.TicketListView.as_view(), name='list_ticket'),
    path('update_ticket/<int:pk>', views.TicketUpdateView.as_view(), name='update_ticket'),
    path('delete_ticket/<int:pk>', views.TicketDeleteView.as_view(), name='delete_ticket'),
    path('detail_ticket/<int:pk>', views.TicketDetailView.as_view(), name='detail_ticket'),
]