from django.db import models


class Ticket(models.Model):
    products = models.CharField(max_length=30)
    category = models.CharField(max_length=30)
    description = models.TextField()

    def __str__(self):
        return self.products
