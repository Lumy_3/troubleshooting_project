from django import forms
from django.forms import TextInput
from ticket.models import Ticket


class TicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = '__all__'
        widgets = {
            'products': TextInput(attrs={'placeholder': 'Please insert a new ticket', 'class': 'form-control'}),
            'category': TextInput(
                attrs={'placeholder': 'Please insert a new category', 'class': 'form-control'}),
            'description': TextInput(
                attrs={'placeholder': 'Please insert your question', 'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(TicketForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        name = cleaned_data.get('brand')
        all_tickets = Ticket.objects.all()
        for ticket in all_tickets:
            if ticket.products == name:
                msg = 'This category is already saved'
                self._errors['brand'] = self.error_class([msg])
