from django.contrib import admin

from information.models import Information, Questions

admin.site.register(Information)
admin.site.register(Questions)

