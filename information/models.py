from django.db import models

from brand.models import Brand
from products.models import Products
from type.models import Type


class Information(models.Model):
    model = models.CharField(max_length=100)
    question = models.CharField(max_length=100)
    info_text = models.TextField()
    brand = models.ForeignKey(Brand, null=True, on_delete=models.CASCADE)
    products = models.ForeignKey(Products, null=True, on_delete=models.CASCADE)
    type = models.ForeignKey(Type, null=True, on_delete=models.CASCADE)
    # questions = models.ForeignKey(Questions, null=True, on_delete=models.CASCADE)
    image = models.CharField(max_length=100)

    def __str__(self):
        return self.model


class Questions(models.Model):
    info_questions = models.CharField(max_length=100)
    brand = models.ForeignKey(Brand, null=True, on_delete=models.CASCADE)
    products = models.ForeignKey(Products, null=True, on_delete=models.CASCADE)
    type = models.ForeignKey(Type, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.info_questions
