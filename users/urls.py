from django.urls import path
from users import views

urlpatterns = [
    path('create_user/', views.UserCreateView.as_view(), name='add_user')
]
