from django.db import models

from brand.models import Brand
from products.models import Products


class Type(models.Model):
    type = models.CharField(max_length=100)
    brand = models.ForeignKey(Brand, null=True, on_delete=models.CASCADE)
    products = models.ForeignKey(Products, null=True, on_delete=models.CASCADE)
    image = models.CharField(max_length=100)

    def __str__(self):
        return self.type
