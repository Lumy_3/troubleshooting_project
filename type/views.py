from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from type.models import Type


class TypeCreateView(CreateView):
    template_name = 'type/add_type.html'
    model = Type
    fields = '__all__'
    # form_class = ProductsForm
    context_object_name = 'type'
    success_url = reverse_lazy('new_type')


class TypeListView(ListView):
    template_name = 'type/list_type.html'
    model = Type
    context_object_name = 'types'


class TypeUpdateView(UpdateView):
    template_name = 'type/update_type.html'
    model = Type
    context_object_name = 'type'
    fields = '__all__'
    success_url = reverse_lazy('list_type')


class TypeDeleteView(DeleteView):
    template_name = 'type/delete_type.html'
    model = Type
    context_object_name = 'delete_type'
    success_url = reverse_lazy('list_type')


class TypeDetailView(DetailView):
    template_name = 'type/detail_type.html'
    model = Type
    context_object_name = 'detail_type'


def get_products_per_type(request, pk):
    all_products_per_type = Type.objects.filter(products_id=pk)
    context = {
        'all_products_per_type': all_products_per_type
    }

    return render(request, 'home/all_products_per_type.html', context)
