from django import forms
from django.forms import TextInput
from brand.models import Brand
from products.models import Products


class ProductsForm(forms.ModelForm):
    class Meta:
        model = Products
        fields = '__all__'
        widgets = {
            'name_of_product': TextInput(attrs={'placeholder': 'Please insert the name of product',
                                                'class': 'form-control'}),
            'model_of_product': TextInput(
                attrs={'placeholder': 'Please insert a new product', 'class': 'form-control'}),
            'troubleshooting': TextInput(
                attrs={'placeholder': 'Please insert a troubleshooting details', 'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(ProductsForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        name = cleaned_data.get('name')
        all_products = Brand.objects.all()
        for product in all_products:
            if product.name_of_product == name:
                msg = 'This category is already saved'
                self._errors['name'] = self.error_class([msg])
