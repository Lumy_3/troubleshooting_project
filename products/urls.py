from django.urls import path
from products import views
urlpatterns = [
    path('products/', views.ProductCreateView.as_view(), name='new_product'),
    path('list_of_product/', views.ProductListView.as_view(), name='list_product'),
    path('update_product/ <int:pk>', views.ProductUpdateView.as_view(), name='update_product'),
    path('delete_product/<int:pk>', views.ProductDeleteView.as_view(), name='delete_product'),
    path('detail_product/<int:pk>', views.ProductDetailView.as_view(), name='detail_product'),
    path('info/<int:pk>', views.get_info_per_product, name='info'),
]