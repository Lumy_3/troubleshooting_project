from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from information.models import Information
from products.forms import ProductsForm
from products.models import Products


class ProductCreateView(CreateView):
    template_name = 'products/add_product.html'
    model = Products

    form_class = ProductsForm
    context_object_name = 'product'
    success_url = reverse_lazy('new_product')


class ProductListView(ListView):
    template_name = 'products/list_product.html'
    model = Products
    context_object_name = 'products'


class ProductUpdateView(UpdateView):
    template_name = 'products/update_product.html'
    model = Products
    context_object_name = 'product'
    fields = '__all__'
    success_url = reverse_lazy('list_product')


class ProductDeleteView(DeleteView):
    template_name = 'products/delete_product.html'
    model = Products
    context_object_name = 'delete_product'
    success_url = reverse_lazy('list_product')


class ProductDetailView(DetailView):
    template_name = 'products/detail_product.html'
    model = Products
    context_object_name = 'detail_product'


def get_info_per_product(request, pk):
    all_info_per_product = Information.objects.filter(products_id=pk)
    context = {
        'all_info_per_product': all_info_per_product
    }

    return render(request, 'home/all_info_per_products.html', context)
