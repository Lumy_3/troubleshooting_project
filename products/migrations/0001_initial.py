# Generated by Django 3.2.5 on 2021-07-27 21:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('brand', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Products',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_of_product', models.CharField(max_length=100)),
                ('model_of_product', models.CharField(max_length=100)),
                ('troubleshooting', models.TextField()),
                ('brand', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='brand.brand')),
            ],
        ),
    ]
