from django.db import models
from brand.models import Brand


class Products(models.Model):
    name_of_product = models.CharField(max_length=100)
    brand = models.ForeignKey(Brand, null=True, on_delete=models.CASCADE)
    image = models.CharField(max_length=100)

    def __str__(self):
        return self.name_of_product
